/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"

#include "floatingeye.h"

void FloatingEye::RegisterObject(Context* context)
{
    context->RegisterFactory<FloatingEye>();
}

FloatingEye::FloatingEye(Context* context): Character(context),
    modelNode_{},
    smoothTargetPosition_{ Vector3::ZERO }
{
}

void FloatingEye::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Character::OnNodeSet(node);

    node_->SetName("FloatingEye");
    modelNode_ = node_->CreateChild("ModelNode");
    ballModel_ = modelNode_->CreateComponent<StaticModel>();
    corneaModel_ = modelNode_->CreateComponent<StaticModel>();
}

void FloatingEye::DelayedStart()
{
    Character::DelayedStart();

    if (!node_)
        return

    rigidBody_->SetMass(1.0f);
    rigidBody_->SetFriction(0.0f);

    ballModel_->SetModel(RES(Model, "Models/FloatingEye.mdl"));
    ballModel_->SetMaterial(RES(Material, "Materials/FloatingEye.xml"));
    ballModel_->SetCastShadows(true);

    corneaModel_->SetModel(RES(Model, "Models/Cornea.mdl"));
    corneaModel_->SetMaterial(RES(Material, "Materials/Cornea.xml"));
    corneaModel_->SetCastShadows(false);
}

void FloatingEye::Update(float timeStep)
{
    if (!modelNode_)
        return;

    modelNode_->SetPosition(Vector3{ GetSubsystem<MasterControl>()->Sine(0.9f,  -0.023f, 0.023f,  variator_ * M_PI * 2.0f),
                                     GetSubsystem<MasterControl>()->Sine(1.0f,  -0.05f,  0.075f, -variator_ * M_PI * 2.0f),
                                     GetSubsystem<MasterControl>()->Sine(0.91f, -0.023f, 0.023f,  variator_ * M_PI) });

    Vector3 targetPosition{ GetSubsystem<Game>()->world_.ko->GetPosition() };

    Quaternion rotation{ node_->GetWorldRotation() };
    Quaternion aimRotation;
    if (aimRotation.FromLookRotation(targetPosition - node_->GetWorldPosition()))
        node_->SetRotation(rotation.Slerp(aimRotation, 1.5f * timeStep));
}
