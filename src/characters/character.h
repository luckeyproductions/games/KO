/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef KO_H
#define KO_H

#include "../controllable.h"

enum KOActions{ RUN, HACK, /*BASH, KICK,*/ CAST, LAUGH };
enum MaterialSlot{ MAT_UNDERPANTS = 0, MAT_SKIN, MAT_LIPS, MAT_HAIR, MAT_GUMS, MAT_TEETH, MAT_EYEWHITE, MAT_IRIS, MAT_PUPIL, MAT_CORNEA, MAT_BROW };

class Character : public Controllable
{
    DRY_OBJECT(Character, Controllable);
    friend class KOCam;

public:
    static void RegisterObject(Context *context);
    Character(Context* context);
    void Start() override;
    void DelayedStart() override;

    void EquipRightHand();
    void EquipLeftHand();
    void Randomize();

    void SetPosition(const Vector3& pos) { kinematicController_->Warp(pos); }

    float GetHealth(){ return health_; }
    void Hit(float damage, int ownerID);
    Vector3 GetLinearVelocity() { return rigidBody_->GetLinearVelocity(); }
    
protected:
    void HandleAction(int actionId);

private:
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;
    void Update(float timeStep) override;
    void Hack();
    void Cast();
    void Laugh();

    float initialHealth_;
    float health_;
    int firstHitBy_;
    int lastHitBy_;

    KinematicCharacterController* kinematicController_;

    StaticModel* leftHand_;
    StaticModel* rightHand_;

    SharedPtr<Material> skinMaterial_;
    SharedPtr<Material> hairMaterial_;
    SharedPtr<Material> irisMaterial_;

    HashMap<int, SharedPtr<AnimatedModel> > equipment_;
};

#endif // KO_H
