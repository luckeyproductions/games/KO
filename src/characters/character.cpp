/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

//#include "mastercontrol.h"
#include "../kocam.h"

#include "character.h"

void Character::RegisterObject(Context* context)
{
    context->RegisterFactory<Character>();
}

Character::Character(Context* context): Controllable(context),
    initialHealth_{ 1.f },
    health_{ initialHealth_ },
    firstHitBy_{ 0 },
    lastHitBy_{ 0 },
    kinematicController_{ nullptr },
    skinMaterial_{ nullptr },
    hairMaterial_{ nullptr },
    irisMaterial_{ nullptr }
{
    actionInterval_[HACK] = .23f;
//    actionInterval_[CAST] = 1.f;
    actionInterval_[LAUGH] = Random(23.f);
}

void Character::Start()
{
    Controllable::Start();

    collisionShape_->SetCapsule(0.3f, 0.5f, Vector3::UP * 0.25f);
    kinematicController_ = node_->CreateComponent<KinematicCharacterController>();
//    kinematicController_->SetCollisionLayerAndMask(ColLayer_Kinematic, ColMask_Kinematic);
    kinematicController_->SetStepHeight(1/8.f);
//    kinematicController_->SetGravity(GRAVITY);
    kinematicController_->SetMaxSlope(40.f);

    actionSince_[HACK] = actionInterval_[HACK];
    actionSince_[CAST] = actionInterval_[CAST];
    actionSince_[LAUGH] = 1e-5f;

//    if (NETWORK->IsServerRunning())
//    {
    node_->SetRotation(Quaternion{ Random(360.0f), Vector3::UP });
    model_->SetModel(RES(Model, "Models/Character/Male.mdl"));
    model_->SetMaterial(MAT_SKIN, skinMaterial_);
    //    model_->SetMaterial(1, MC->GetMaterial, "Teeth"));
    //    model_->SetMaterial(2, MC->GetMaterial, "Gums"));
    //    model_->SetMaterial(3, MC->GetMaterial, "Underpants"));

    model_->SetMaterial(MAT_LIPS, skinMaterial_);
    //    model_->SetMaterial(5, MC->GetMaterial, "EyeWhite"));
    model_->SetMaterial(MAT_CORNEA, RES(Material, "Materials/Cornea.xml"));
    model_->SetMaterial(MAT_IRIS, irisMaterial_);
    model_->SetMaterial(MAT_PUPIL, RES(Material, "Materials/Darkness.xml"));
    model_->SetMaterial(MAT_HAIR, hairMaterial_);
    model_->SetMaterial(MAT_BROW, hairMaterial_);

    for (unsigned e{ 0u }; e < 4u; ++e)
    {
        equipment_[e] = node_->CreateComponent<AnimatedModel>(REPLICATED);
    }

    rightHand_ = node_->GetChild("Hand.R", true)->CreateComponent<StaticModel>();
    rightHand_->SetCastShadows(true);

    leftHand_ = node_->GetChild("Wrist.L", true)->CreateComponent<StaticModel>();
    leftHand_->SetCastShadows(true);

//    EquipLeftHand();
//    EquipRightHand();

    animCtrl_->SetTime("Animations/Idle.ani", Random(animCtrl_->GetLength("Animations/Idle.ani")));
}

void Character::DelayedStart()
{
    Controllable::DelayedStart();

    Randomize();
}

void Character::Randomize()
{
//    node_->SetScale(Vector3(1.0f, Random(0.95f, 1.1f), 1.0f));
    model_->SetModel(RES(Model, String{ "Models/Character/" } + (true ? "Male" : "Female") + ".mdl"));
    //Randomize colors
    Materials* mat{ GetSubsystem<Materials>() };
    Tone skinTone{ Tone::Pick() };
    Tint hairTint{ Tint::Pick(skinTone) };
    Shade eyeColor{ skinTone.Eyes() };

    model_->SetMaterial(MAT_SKIN, mat->Skin(skinTone) );
    model_->SetMaterial(MAT_LIPS, mat->Skin(skinTone.Lips()));
    model_->SetMaterial(MAT_GUMS, mat->Skin(skinTone.Gums()));
    model_->SetMaterial(MAT_TEETH, mat->Paint(Shade{ 4, Random(1, 3), 7 }));
    model_->SetMaterial(MAT_EYEWHITE, RES(Material, "Resources/Materials/Gloss.xml"));
    model_->SetMaterial(MAT_CORNEA, RES(Material, "Materials/Cornea.xml"));
    model_->SetMaterial(MAT_IRIS, mat->Cloth(eyeColor));
    model_->SetMaterial(MAT_PUPIL, RES(Material, "Materials/Darkness.xml"));
    model_->SetMaterial(MAT_HAIR, mat->Hair(hairTint));
    hairMaterial_ = model_->GetMaterial(MAT_HAIR);
    model_->SetMaterial(MAT_BROW, hairMaterial_);

    for (unsigned e{ 0u }; e < 4u; ++e)
    {
        AnimatedModel* equipment{ equipment_[e].Get() };

        if (!equipment)
            continue;

        switch (e)
        {
        case 0: // Random hair model
            switch(Random(3))
            {
            case 0: default: equipment->SetModel(nullptr);
            break;
            case 1:
                equipment->SetModel(RES(Model, "Models/Character/Hair/Mohawk.mdl"));
            break;
            case 2:
                equipment->SetModel(RES(Model, "Models/Character/Hair/Horns.mdl"));
            break;
            }

        break;
        case 1: // Shirt model
            equipment->SetModel(RES(Model, "Models/Character/Clothes/MaleShirt.mdl"));
        break;
        case 2: // Random pants model
            switch(Random(2))
            {
            case 0:
                equipment->SetModel(RES(Model, "Models/Character/Clothes/LoosePants.mdl"));
            break;
            case 1:
                equipment->SetModel(RES(Model, "Models/Character/Clothes/Shorts.mdl"));
            break;
            }
        break;
        case 3: // Random beard model
            switch(Random(3))
            {
            case 0: default: equipment->SetModel(nullptr);
            break;
            case 1:
                equipment->SetModel(RES(Model, "Models/Character/Hair/Beard.mdl"));
            break;
            case 2:
                equipment->SetModel(RES(Model, "Models/Character/Hair/Fumanchu.mdl"));
            break;
            }
        break;
        case 4: // Shield model
            equipment->SetModel(RES(Model, "Models/Character/Items/Shield.mdl"));
        break;
        case 5: // Sword model
            equipment->SetModel(RES(Model, "Models/Character/Items/Sword.mdl"));
        break;
        }

        Model* model{ equipment->GetModel() };

        if (model)
        {
            switch(e)
            {
            case 0: equipment->SetMaterial(hairMaterial_);
            break;
            case 1: equipment->SetMaterial(RES(Material, "Materials/Cloth.xml")->Clone());
            break;
            case 2: equipment->SetMaterial(RES(Material, "Materials/Pants.xml")->Clone());
            break;
            case 3: equipment->SetMaterial(hairMaterial_);
            break;
            case 4:
                equipment->SetMaterial(0, RES(Material, "Materials/Metal.xml"));
                equipment->SetMaterial(1, RES(Material, "Materials/Leather.xml"));
            break;
            case 5: equipment->SetMaterial(RES(Material, "Materials/Metal.xml"));
            break;
            default:
            break;
            }

            equipment->SetCastShadows(true);
        }
    }

    for (int e : { 1, 2 })
    {
        if (!equipment_.Contains(e))
            continue;

        Color color{ LucKey::RandomColor() };
        Vector3 colorHSV{ color.ToHSV() };
        Color specColor{};
        specColor.FromHSV(LucKey::Cycle(colorHSV.x_ - 0.5f, 0.0f, 1.0f),
                          Min(0.5f, colorHSV.y_), colorHSV.z_ * 0.25f);

        equipment_[e]->GetMaterial()->SetShaderParameter("MatDiffColor", color);
        equipment_[e]->GetMaterial()->SetShaderParameter("MatSpecColor", specColor);
    }

    //Randomize morphs
    for (unsigned m{ 0u }; m < model_->GetNumMorphs(); ++m)
    {
        const StringHash morphNameHash{ model_->GetMorphs().At(m).nameHash_ };
        float newWeight{ Random(-0.5f, 1.0f) };
        model_->SetMorphWeight(m, newWeight);

        //Apply morphs to equipment
        for (AnimatedModel* model: equipment_.Values())
            for (const ModelMorph& morph: model->GetMorphs())
                if (morph.nameHash_ == morphNameHash)
                    model->SetMorphWeight(morphNameHash, newWeight);
    }
}

void Character::FixedUpdate(float timeStep)
{
    if (!rigidBody_/* || !NETWORK->IsServerRunning()*/)
        return;

    //Movement values
    const float thrust{ 1.0f };

    //Apply movement
    const Vector3 force{ move_ * thrust * timeStep * (1.0f + 1.3f * actions_[RUN]) };
    kinematicController_->SetWalkDirection(force);
}

void Character::FixedPostUpdate(float timeStep)
{
    node_->SetPosition(kinematicController_->GetPosition());
}

void Character::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (actionSince_[LAUGH] > actionInterval_[LAUGH])
        Laugh();

    if (INPUT->GetKeyPress(KEY_RETURN))
        Randomize();

    //Update animation
    if (rigidBody_->GetLinearVelocity().Length() > 0.1f)
    {
        animCtrl_->Play("Animations/Walk.ani", 0, true, 0.23f);
        animCtrl_->SetSpeed("Animations/Walk.ani", rigidBody_->GetLinearVelocity().Length() * .8f);
        animCtrl_->SetStartBone("Animations/Walk.ani", "RootBone");

    }
    else
    {
        animCtrl_->PlayExclusive("Animations/Idle.ani", 0, true, 0.23f);
        //Randomize on first use
        if (animCtrl_->GetTime("Animations/Idle.ani") == 0.0f)
            animCtrl_->SetTime("Animations/Idle.ani", Random(animCtrl_->GetLength("Animations/Idle.ani")));

        animCtrl_->SetStartBone("Animations/Idle.ani", "RootBone");
    }

    if (move_.Length())
        AlignWithMovement(timeStep);
}

void Character::EquipRightHand()
{
    rightHand_->SetModel(RES(Model, "Models/Character/Items/Sword.mdl"));
    rightHand_->SetMaterial(RES(Material, "Materials/Metal.xml"));
}

void Character::EquipLeftHand()
{
    leftHand_->SetModel(RES(Model, "Models/Character/Items/Shield.mdl"));
    leftHand_->SetMaterial(1, RES(Material, "Materials/Leather.xml"));
    leftHand_->SetMaterial(0, RES(Material, "Materials/Metal.xml"));
}

void Character::HandleAction(int actionId)
{
    switch(actionId)
    {
    case HACK:
        if (actionSince_[HACK] > actionInterval_[HACK])
            Hack();
        break;
    case CAST:
        if (actionSince_[CAST] > actionInterval_[CAST])
            Cast();
        break;
    default: break;
    }
}

void Character::Hack()
{
    const String jabAnim{ "Animations/Jab_R.ani" };
    actionSince_[HACK] = 0.0f;
    animCtrl_->Play(jabAnim, 1, false, 0.13f);
    animCtrl_->SetSpeed(jabAnim, 1.8f);
    animCtrl_->SetTime(jabAnim, 0.0f);
    animCtrl_->SetAutoFade(jabAnim, 0.1f);

    if (animCtrl_->IsPlaying("Animations/Walk.ani"))
        animCtrl_->SetStartBone(jabAnim, "UpperBack");
    else
        animCtrl_->SetStartBone("Animations/Jab_R.ani", "RootBone");

    PlaySound(RES(Sound, "Samples/KO/Swing" + String{ Random(3) + 1 } + ".ogg" ));
}

void Character::Cast()
{
    actionSince_[CAST] = 0.0f;

    PlaySound(RES(Sound, "Samples/KO/Spell1.ogg"));
}

void Character::Laugh()
{
    actionSince_[LAUGH] = 0.1f;
    actionInterval_[LAUGH] = Random(5.0f, 23.0f);
    PlaySound(RES(Sound, "Samples/KO/Laugh" + String{ Random(7) + 1 } + ".ogg"));
}
