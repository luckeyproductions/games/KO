/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINMENU_H
#define MAINMENU_H

#include "../mastercontrol.h"

#include "tome.h"

class Overlay: public Object
{
    DRY_OBJECT(Overlay, Object);

public:
    static void RegisterObject(Context* context);

    Overlay(Context* context);

    void HideMain();

private:
    void CreateScene();

    SharedPtr<Scene> overlayScene_;
    Tome* tome_;
    Node* cameraNode_;
    Node* tableNode_;
};

#endif // MAINMENU_H
