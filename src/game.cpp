/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "characters/character.h"
#include "characters/floatingeye.h"
#include "environment/block.h"
#include "environment/dungeon.h"
#include "environment/firepit.h"
#include "environment/frop.h"
#include "environment/portal.h"
#include "kocam.h"

#include "ui/gui.h"
#include "inputmaster.h"
#include "jukebox.h"
#include "player.h"

#include "game.h"

Game::Game(Context* context): Object(context),
    world_{},
    scene_{ nullptr },
    status_{ GS_MAIN }
{
    context_->RegisterFactory<Dungeon>();
    context_->RegisterFactory<Portal>();
    Block::RegisterObject(context_)  ;
    FloatingEye::RegisterObject(context_);
    FirePit::RegisterObject(context_);
    Frop::RegisterObject(context_);
    Character::RegisterObject(context_);

    CreateScene();

    context_->RegisterSubsystem<JukeBox>()->Play("TenPenny Joke - Chaos Engine.ogg", false);
}

void Game::StartNew()
{
    GetSubsystem<GUI>()->GetOverlay()->HideMain();

    GetSubsystem<JukeBox>()->Play("yoitrax - warrior.ogg");

    SetStatus(GS_PLAY);
    scene_->SetTimeScale(1.f);
}

void Game::CreateScene()
{
    scene_ = MakeShared<Scene>(context_);

    scene_->LoadXML(RES(XMLFile, "Maps/Birdhouse.xml")->GetRoot());
    scene_->SetTimeScale(0.f);

    for (Node* n: scene_->GetChildrenWithTag("Portal", true))
        n->CreateComponent<Portal>();

//    scene_->CreateComponent<Octree>(LOCAL);
    //octree->SetSize(BoundingBox(Vector3(-10000, -100, -10000), Vector3(10000, 1000, 10000)), 1024);
    /*PhysicsWorld* physicsWorld = */scene_->CreateComponent<PhysicsWorld>(LOCAL);
//    scene_->CreateComponent<DebugRenderer>();

//    Zone* zone{ scene_->CreateComponent<Zone>() };
//    zone->SetBoundingBox(BoundingBox{ -1000.0f * Vector3::ONE, 1000.0f * Vector3::ONE });
//    zone->SetAmbientColor(Color::WHITE);

//    //Create a directional light to the world_. Enable cascaded shadows on it
//    Node* lightNode{ scene_->CreateChild("DirectionalLight", LOCAL) };
//    lightNode->SetDirection(Vector3{ 0.5f, -1.0f, 0.0f });
//    Light* light{ lightNode->CreateComponent<Light>() };
//    light->SetLightType(LIGHT_DIRECTIONAL);
//    light->SetBrightness(2.3f);
//    light->SetColor(Color{ 1.0f, 0.8f, 0.7f });
//    light->SetCastShadows(true);
//    light->SetShadowIntensity(0.5f);
//    light->SetSpecularIntensity(0.0f);
//    light->SetShadowBias(BiasParameters{ 0.00025f, 0.5f });
//    light->SetShadowResolution(0.23f);
//    //Set cascade splits at 10, 50, 200 world unitys, fade shadows at 80% of maximum shadow distance
//    light->SetShadowCascade(CascadeParameters{ 7.0f, 23.0f, 42.0f, 500.0f, 0.8f });

    world_.ko = scene_->CreateChild("KO")->CreateComponent<Character>();
    world_.ko->SetPosition({ -5.f, 0.f, -5.f });

    SharedPtr<Player> player{ new Player{ 1, context_ } };
    players_.Push(player);
    INPUTMASTER->SetPlayerControl(player, world_.ko);

//    world_.dungeon_ = scene_->CreateChild("Dungeon", LOCAL)->CreateComponent<Dungeon>();

    Node* camNode{ scene_->CreateChild("Camera", LOCAL) };
    world_.camera = camNode->CreateComponent<KOCam>(LOCAL);
    world_.camera->Lock(world_.ko->GetNode());
    player->SetCamera(world_.camera);
}

void Game::TogglePause()
{
    if (status_ == GS_PLAY)
    {
        scene_->SetTimeScale(0.f);
        status_ = GS_PAUSED;
    }
    else if (status_ == GS_PAUSED)
    {
        scene_->SetTimeScale(1.f);
        status_ = GS_PLAY;
    }
}

Vector<SharedPtr<Player> > Game::GetPlayers() const
{
    return players_;
}


Player* Game::GetPlayer(int playerID) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerID)
            return p;
    }

    return nullptr;
}
