
#ifndef JUKEBOX_H
#define JUKEBOX_H

#include "mastercontrol.h"



class JukeBox: public Object
{
    DRY_OBJECT(JukeBox, Object);

public:
    JukeBox(Context* context);

    void Play(String song, bool loop = true);

private:
    void HandleMusicFaded(StringHash eventType, VariantMap& eventData);

    Node* musicNode_;
    Vector<SoundSource*> tracks_;
};

DRY_EVENT(E_MUSICFADED, MusicFaded)
{
}

#endif // JUKEBOX_H
