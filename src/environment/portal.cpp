
#include "portal.h"

Portal::Portal(Context* context): Component(context)
{
}

void Portal::OnSetEnabled() { Component::OnSetEnabled(); }
void Portal::OnNodeSet(Node* node)
{
    if (!node)
        return;

    sideA_ = node_->GetChild("SideA");
    sideB_ = node_->GetChild("SideB");

    RigidBody* rb{ node_->GetComponent<RigidBody>() };

    if (!sideA_ || !sideB_ || !rb)
    {
        Remove();
        return;
    }

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Portal, HandlePortalCollision));

}

void Portal::HandlePortalCollision(StringHash eventType, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };

    if (otherNode)
    {
        KinematicCharacterController* characterController{ otherNode->GetComponent<KinematicCharacterController>() };

        if (!characterController)
            return;

        float toA{ otherNode->GetWorldPosition().DistanceSquaredToPoint(sideA_->GetWorldPosition()) };
        float toB{ otherNode->GetWorldPosition().DistanceSquaredToPoint(sideB_->GetWorldPosition()) };

        if (toA < toB)
        {
            characterController->Warp(sideB_->GetWorldPosition());
        }
        else
        {
            characterController->Warp(sideA_->GetWorldPosition());
        }
    }
}

void Portal::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Portal::OnMarkedDirty(Node* node) {}
void Portal::OnNodeSetEnabled(Node* node) {}
bool Portal::Save(Serializer& dest) const { return Component::Save(dest); }
bool Portal::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Portal::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Portal::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Portal::GetDependencyNodes(PODVector<Node*>& dest) {}
void Portal::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
