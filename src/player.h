/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "luckey.h"

class GUI3D;
class KOCam;
class Controllable;

class Player: public Object
{
    DRY_OBJECT(Player, Object);

public:
    Player(int playerId, Context* context);

    Vector3 GetPosition();
    Controllable* GetControllable();

    int GetPlayerId() const { return playerId_; }
    void Die();
    void Respawn();

    bool IsAlive() const noexcept { return alive_; }
    bool IsHuman() const noexcept { return human_; }

    void SetCamera(KOCam* camera) { activeCamera_ = camera; }
    KOCam* GetCamera() const { return activeCamera_; }

private:
    int playerId_;
    bool human_;
    bool alive_;

    unsigned score_;
    unsigned flightScore_;
    int multiplier_;

    KOCam* activeCamera_;

    void SetScore(int points);
    Vector3 Sniff(float playerFactor, bool taste);
};

#endif // PLAYER_H
