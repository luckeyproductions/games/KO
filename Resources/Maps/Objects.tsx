<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.1" tiledversion="2018.08.22" name="Objects" tilewidth="32" tileheight="32" tilecount="4" columns="4">
 <tileoffset x="-16" y="16"/>
 <image source="Objects.png" width="128" height="32"/>
 <tile id="0">
  <properties>
   <property name="Player" value=""/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="FloatingEye" value=""/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="FirePit" value=""/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="Plant" value=""/>
  </properties>
 </tile>
</tileset>
