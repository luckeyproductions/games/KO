class Hider : ScriptObject
{
    void Update(float timeStep)
    {
		if (node is null)
			return;
		if (scene is null)
			return;

		bool enable = true;
		Node@ ko = scene.GetChild("KO");
		if (ko !is null)
		{
			Vector3 koPos = ko.worldPosition;
			if (koPos.y > 5 && node.worldPosition.y - koPos.y > 0.5)
				enable = false;
		}
        node.GetChild("Graphics").enabled = enable;
    }
}
