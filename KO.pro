include(src/KO.pri)

TARGET = ko

LIBS += $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++17 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

unix {
    isEmpty(DATADIR) {
        DATADIR = ~/.local/share
    }
    DEFINES += DATADIR=\\\"$${DATADIR}/ko\\\"

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/ko/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = ko.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = ko.desktop
    INSTALLS += desktop
}
